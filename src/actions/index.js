import userActions from './userActions'
import loginActions from './loginActions'

const allActions = {
    userActions,
    loginActions
}

export default allActions
