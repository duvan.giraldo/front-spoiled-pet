import * as Constants from '../constants/loginConstants'

const logIn = (user) => {
    return {
        type: Constants.LOG_IN,
        payload: user
    }
}

const logOut = () => {
    return {
        type: Constants.LOG_OUT
    }
}

const errorLogIn = () => {
    return {
        type: Constants.ERROR_LOG_IN
    }
}

export default {
    logIn,
    logOut,
    errorLogIn
}