import * as Constants from '../constants/userConstants'

const setUser = (userObj) => {
    return {
        type: Constants.ADD_USER,
        payload: userObj
    }
}

const logOut = () => {
    return {
        type: Constants.REMOVE_USER
    }
}

export default {
    setUser,
    logOut
}