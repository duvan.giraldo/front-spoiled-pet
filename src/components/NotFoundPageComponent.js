import React from 'react'

function NotFoundPageComponent() {

    return (
        <h1>Not found page</h1>
    );
}

export default NotFoundPageComponent;