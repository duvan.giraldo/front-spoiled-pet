import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import allActions from '../actions'

const LoginComponent = () => {
    const loginState = useSelector(state => state.login)
    const dispatch = useDispatch()

    const user = { name: "Rei" }

    useEffect(() => {
        dispatch(allActions.loginActions.logIn(user))
    }, [])

    return (
        <div>
            {
                loginState.loggedIn ?
                    <>
                        <h1>Hello you are logged In, {loginState.user.name}</h1>
                        <button onClick={() => dispatch(allActions.loginActions.logOut())}>Logout</button>
                    </>
                    :
                    <>
                        <h1>Not Login</h1>
                        <button onClick={() => dispatch(allActions.loginActions.logIn(user))}>Login as Rei</button>
                    </>
            }

            <div>
            <form>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block">Submit</button>
                <p className="forgot-password text-right">
                    Forgot <a href="/">password?</a>
                </p>
            </form>
            </div>

        </div>
    );
}

export default LoginComponent;
