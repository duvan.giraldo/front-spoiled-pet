import user from './userReducer'
import login from './loginReducer'
import {combineReducers} from 'redux'

const rootReducer = combineReducers({
    user,
    login
})

export default rootReducer