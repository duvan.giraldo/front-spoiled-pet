import * as Constants from '../constants/loginConstants'

const login = (state = {}, action) => {
    switch (action.type) {
        case Constants.LOG_IN:
            return {
                ...state,
                user: action.payload,
                loggedIn: true
            }
        case Constants.LOG_OUT:
            return {
                ...state,
                user: null,
                loggedIn: false
            }
        case Constants.ERROR_LOG_IN:
            return {
                ...state,
                user: {},
                loggedIn: false
            }
        default:
            return state
    }
}

export default login;