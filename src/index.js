import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';
import { createStore } from 'redux'
import rootReducer from './reducers'
import './custom.scss';
import { BrowserRouter as Router } from "react-router-dom";


const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
    <Provider store={store}>
        <Router><App /></Router>
    </Provider>
    , document.getElementById('root'));

serviceWorker.unregister();
